import json
from requests import post

record = {
    "auth": "xxxxx",
    "time_entries": {
        "created_with": "target-toggl",
        "description": "test",
        "tags": [],
        "wid": ,
        "
    }
}

endpoint = 'https://www.toggl.com/api/v8/'

# # file for each type
# valid_types = [
#     'clients',
#     'groups',
#     'project_users',
#     'projects',
#     'tags',
#     'tasks',
#     'time_entries',
#     'me',
#     'reset_token',
#     'signups',
#     'workspaces',
#     'workspace_users'
# ]

headers = {'Content-Type': 'application/json'}
auth = (record['auth'], 'api_token')
data = str(record['type'])

r = post(url, data = data, auth = auth, headers = headers)

# required:
# - description
# - any of wid, pid, or tid
# - start
# - duration

# data = '''{
#     "time_entry": {
#         "created_with": "target-toggl",
#         "description": {0},
#         "tags": {1},
#         "wid": {2},
#         "pid": {3},
#         "tid": {4},
#         "billable": {5},
#         "start": {6},
#         "stop": {7},
#         "duration": {8}
#     }
# }'''.format(description, tags, wid, pid, tid, billable, start, stop, duration)