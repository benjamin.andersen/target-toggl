import json
from requests import post

def load(endpoint, api_token, data):
    root = 'https://www.toggl.com/api/v8/'
    url = root + endpoint
    headers = {'Content-Type': 'application/json'}
    auth = (api_token, 'api_token')

    r = post(url, data = data, auth = auth, headers = headers)